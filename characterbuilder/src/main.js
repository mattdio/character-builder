import Vue from 'vue'
import App from './App.vue'

import CharName from './CharName.vue'
import CharClass from './CharClass.vue'
import CharAbs from './CharAbs.vue'

import VueRouter from 'vue-router'

import "bootstrap/dist/css/bootstrap.min.css"

Vue.use(VueRouter);

const router = new VueRouter({
  mode: 'history',
  base: __dirname,
  routes: [
    { path: '/', component: CharName },
    { path: '/charclass', component: CharClass },
    { path: '/abs', component: CharAbs }
  ]
});

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
